﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackEndTest2
{
    internal static class Program
    {
        public static int maxWater(int[] arr, int n)
        {
            int res = 0;
            for (int i = 1; i < n - 1; i++)
            {
                int left = arr[i];
                for (int j = 0; j < i; j++)
                {
                    left = Math.Max(left, arr[j]);
                }
                int right = arr[i];
                for (int j = i + 1; j < n; j++)
                {
                    right = Math.Max(right, arr[j]);
                }
                res += Math.Min(left, right) - arr[i];
            }
            return res;
        }
        public static void Main(String[] args)
        {
            int[] arr = { 0, 1, 0, 2, 1, 0, 1, 3, 2, 0, 1, 0, 2 };
            int n = arr.Length;

            Console.WriteLine(maxWater(arr, n));
        }
    }
}
