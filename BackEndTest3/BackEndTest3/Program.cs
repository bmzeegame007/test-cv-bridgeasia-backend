﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackEndTest3
{
    internal static class Program
    {
        public class OutputTotal : IEquatable<OutputTotal>, IComparable<OutputTotal>
        {
            public string SizeTicket { get; set; }

            public int moneyTicket { get; set; }

            public override string ToString()
            {
                return "====== Best Solution ======"+ "\n" 
                     + SizeTicket + "\n"
                     + "Total: " + moneyTicket + "$" +"\n"
                    +  "===========================";
            }
            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                OutputTotal objAsPart = obj as OutputTotal;
                if (objAsPart == null) return false;
                else return Equals(objAsPart);
            }
            public int SortByNameAscending(string name1, string name2)
            {

                return name1.CompareTo(name2);
            }

            // Default comparer for Part type.
            public int CompareTo(OutputTotal comparePart)
            {
                // A null value means that this object is greater.
                if (comparePart == null)
                    return 1;

                else
                    return this.moneyTicket.CompareTo(comparePart.moneyTicket);
            }
            public override int GetHashCode()
            {
                return moneyTicket;
            }
            public bool Equals(OutputTotal other)
            {
                if (other == null) return false;
                return (this.moneyTicket.Equals(other.moneyTicket));
            }
            // Should also override == and != operators.
        }

        public static void Main(String[] args)
        {
            int seat = 31;
            int sumS = 0;
            int sumM = 0;
            int sumL = 0;

            List<string> arrSizeCarS = new List<string>() { "S", "5", "5" };
            List<string> arrSizeCarM = new List<string>() { "M", "10", "8" };
            List<string> arrSizeCarL = new List<string>() { "L", "15", "12" };

            List<OutputTotal> outputTotal = new List<OutputTotal>();

            int countS = 0;
            int countM = 0;
            int countL = 0;
            int countLM = 0;
            int countLS = 0;
            int countLMS = 0;
            int countMS = 0;
            int money = 0;
            int valresult = 0;
            int solution = 0;
            int totalmoney = 0;

            ////////////// หา L มากที่สุด ////////////
            for (int i = 0; i < arrSizeCarL.Count(); i--)
            {
                sumL = Int32.Parse(arrSizeCarL[1]);
                money = Int32.Parse(arrSizeCarL[2]);
                valresult += sumL;
                countL++;
                
                if (valresult >= seat)
                {
                    totalmoney = money * countL;
                    solution++;
                    Console.WriteLine("====================");
                    Console.WriteLine("Solution : " + solution);
                    Console.WriteLine("L x " + countL);
                    Console.WriteLine("Total: " + totalmoney + "$");
                    Console.WriteLine("====================");
                    outputTotal.Add(new OutputTotal() { SizeTicket = "L x " + countL, moneyTicket = totalmoney });
                    valresult = 0;
                    break;
                }
                
            }

            ////////////// หา M มากที่สุด ////////////
            for (int i = 0; i < arrSizeCarM.Count(); i--)
            {
                sumM = Int32.Parse(arrSizeCarM[1]);
                money = Int32.Parse(arrSizeCarM[2]);
                valresult += sumM;
                countM++;
                if (valresult >= seat)
                {
                    totalmoney = money * countM;
                    solution++;
                    Console.WriteLine("Solution : " + solution);
                    Console.WriteLine("M x " + countM);
                    outputTotal.Add(new OutputTotal() { SizeTicket = "M x " + countM, moneyTicket = totalmoney });
                    Console.WriteLine("Total: " + totalmoney + "$");
                    Console.WriteLine("====================");
                    valresult = 0;
                    break;
                }

            }

            ////////////// หา S มากที่สุด ////////////
            for (int i = 0; i < arrSizeCarS.Count(); i--)
            {
                sumS = Int32.Parse(arrSizeCarS[1]);
                money = Int32.Parse(arrSizeCarS[2]);
                valresult += sumS;
                countS++;
                if (valresult >= seat)
                {
                    totalmoney = money * countS;
                    solution++;
                    Console.WriteLine("Solution : " + solution);
                    Console.WriteLine("S x " + countS);
                    outputTotal.Add(new OutputTotal() { SizeTicket = "S x " + countS, moneyTicket = totalmoney });
                    Console.WriteLine("Total: " + totalmoney + "$");
                    Console.WriteLine("====================");
                    valresult = 0;
                    break;
                }

            }

            //////////// หา L บวกกัน ///////////

            for (int j = 1; j < countL; j++)
            {
                valresult = 0;
                sumL = Int32.Parse(arrSizeCarL[1]) * j;
                money = Int32.Parse(arrSizeCarL[2]) * j;
                valresult += sumL;
                //////////////// หา M ทั้งหมดใน L ////////////
                if (countLM == 0)
                {
                    for (int m1 = 0; m1 < arrSizeCarM.Count(); m1--)
                    {
                        sumM = Int32.Parse(arrSizeCarM[1]);
                        valresult = valresult + sumM;
                        countLM++;
                        if (valresult >= seat)
                        {
                            totalmoney = money + (Int32.Parse(arrSizeCarM[2]) * countLM);
                            solution++;
                            Console.WriteLine("Solution : " + solution);
                            Console.WriteLine("L x " + j +  " M x " + countLM);
                            Console.WriteLine("Total: " + totalmoney + "$");
                            Console.WriteLine("====================");
                            outputTotal.Add(new OutputTotal() { SizeTicket = "L x " + j + "," + " M x " + countLM, moneyTicket = totalmoney });
                            valresult = 0;
                            break;
                        }
                    }
                }

                if (countLS == 0)
                {
                    //////////////// หา S ทั้งหมดใน L ////////////
                    ///
                    valresult += sumL;
                    for (int s1 = 0; s1 < arrSizeCarS.Count(); s1--)
                    {
                        sumS = Int32.Parse(arrSizeCarS[1]);
                        valresult = valresult + sumS;
                        countLS++;
                        if (valresult >= seat)
                        {
                            totalmoney = money + (Int32.Parse(arrSizeCarS[2]) * countLS);
                            solution++;
                            Console.WriteLine("Solution : " + solution);
                            Console.WriteLine("L x " + j + " S x " + countLS);
                            Console.WriteLine("Total: " + totalmoney + "$");
                            Console.WriteLine("====================");
                            outputTotal.Add(new OutputTotal() { SizeTicket = "L x " + j +","+ " S x " + countLS, moneyTicket = totalmoney });
                            valresult = 0;
                            break;
                        }
                    }
                    //Console.WriteLine(countLS);
                }

                //////////////////// หา L + M /////////
                for (int m2 = 1; m2 < countLM; m2++)
                {
                    valresult = 0;
                    sumM = Int32.Parse(arrSizeCarM[1]) * m2 + sumL;
                    valresult += sumM;
                    //////////////// หา M ทั้งหมดใน L ////////////
                    ///
                    if (valresult >= seat)
                    {
                        totalmoney = money + (Int32.Parse(arrSizeCarM[2]) * m2);
                        solution++;
                        Console.WriteLine("Solution : " + solution);
                        Console.WriteLine("L x " + j + " M x " + m2);
                        outputTotal.Add(new OutputTotal() { SizeTicket = "L x " + j + "," + " M x " + m2, moneyTicket = totalmoney });
                        Console.WriteLine("Total: " + totalmoney + "$");
                        Console.WriteLine("====================");
                        valresult = 0;
                        break;
                    }
                    else
                    {
                        for (int s1 = 0; s1 < arrSizeCarS.Count(); s1--)
                        {
                            sumS = Int32.Parse(arrSizeCarS[1]);
                            valresult = valresult + sumS;
                            countLMS++;
                            if (valresult >= seat)
                            {
                                totalmoney = money + (Int32.Parse(arrSizeCarM[2]) * m2) + (Int32.Parse(arrSizeCarS[2]) * countLMS);
                                solution++;
                                Console.WriteLine("Solution : " + solution);
                                Console.WriteLine("L x " + j  + " M x " + m2 + " S x " + countLMS);
                                outputTotal.Add(new OutputTotal() { SizeTicket = "L x " + j + "," + " M x " + m2 + "," + " S x " + countLMS, moneyTicket = totalmoney });
                                Console.WriteLine("Total: " + totalmoney + "$");
                                Console.WriteLine("====================");
                                valresult = 0;
                                break;
                            }
                        }
                    }
                }

                ////////////////// หา L + S /////////
                for (int s2 = 1; s2 < countLS; s2++)
                {
                    valresult = 0;
                    sumS = Int32.Parse(arrSizeCarS[1]) * s2 + sumL;
                    valresult += sumS;
                    //////////////// หา S ทั้งหมดใน L ////////////
                    ///
                    if (valresult >= seat)
                    {
                        totalmoney = money + (Int32.Parse(arrSizeCarS[2]) * s2);
                        solution++;
                        Console.WriteLine("Solution : " + solution);
                        Console.WriteLine("L x " + j + " S x " + s2);
                        outputTotal.Add(new OutputTotal() { SizeTicket = "L x " + j + ","+ " S x " + s2, moneyTicket = totalmoney });
                        Console.WriteLine("Total: " + totalmoney + "$");
                        Console.WriteLine("====================");
                        break;
                    }
                }
            }

            for (int j = 1; j < countM; j++)
            {
                valresult = 0;
                sumM = Int32.Parse(arrSizeCarM[1]) * j;
                money = Int32.Parse(arrSizeCarM[2]) * j;
                valresult += sumM;
                //////////////// หา S ทั้งหมดใน M ////////////
                if (countMS == 0)
                {
                    for (int s1 = 0; s1 < arrSizeCarS.Count(); s1--)
                    {
                        sumS = Int32.Parse(arrSizeCarS[1]);
                        valresult = valresult + sumS;
                        countMS++;
                        if (valresult >= seat)
                        {
                            totalmoney = money + (Int32.Parse(arrSizeCarS[2]) * countMS);
                            solution++;
                            Console.WriteLine("Solution : " + solution);
                            Console.WriteLine("M x " + j + " S x " + countMS);
                            outputTotal.Add(new OutputTotal() { SizeTicket = "M x " + j + "," +" S x " + countMS, moneyTicket = totalmoney });
                            Console.WriteLine("Total: " + totalmoney + "$");
                            Console.WriteLine("====================");
                            valresult = 0;
                            break;
                        }
                    }
                }

                //////////////////// หา M + S /////////
                for (int s2 = 1; s2 < countMS; s2++)
                {
                    valresult = 0;
                    sumS = Int32.Parse(arrSizeCarS[1]) * s2 + sumM;
                    valresult += sumS;

                    if (valresult >= seat)
                    {
                        valresult = 0;
                        totalmoney = money + (Int32.Parse(arrSizeCarS[2]) * s2);
                        solution++;
                        Console.WriteLine("Solution : " + solution);
                        Console.WriteLine("M x " + j + " S x " + s2);
                        outputTotal.Add(new OutputTotal() { SizeTicket = "M x " + j +","+ " S x " + s2, moneyTicket = totalmoney });
                        Console.WriteLine("Total: " + totalmoney + "$");
                        Console.WriteLine("====================");
                        break;
                    }
                }
            }

            outputTotal.Sort();

             Console.WriteLine(outputTotal[0]);

        }

    }
}
