﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackEndTest1
{
    internal static class Program
    {
        // Driver program to test above function
        public static void Main()
        {
            int[] arrNumber = { 4, 39, 246, 1066 };
            List<string> numtostr = new List<string>(); 
            String[] m = { "", "M", "MM", "MMM" };
            String[] c = { "",  "C",  "CC",  "CCC",  "CD",
                       "D", "DC", "DCC", "DCCC", "CM" };
            String[] x = { "",  "X",  "XX",  "XXX",  "XL",
                       "L", "LX", "LXX", "LXXX", "XC" };
            String[] i = { "",  "I",  "II",  "III",  "IV",
                       "V", "VI", "VII", "VIII", "IX" };
            String ans = "";

            for (int n =0; n<arrNumber.Count(); n++ )
            {
                String thousands = m[arrNumber[n] / 1000];
                String hundreds = c[(arrNumber[n] % 1000) / 100];
                String tens = x[(arrNumber[n] % 100) / 10];
                String ones = i[arrNumber[n] % 10];

                ans = thousands + hundreds + tens + ones;
                numtostr.Add(ans);
            }
            Console.WriteLine("[{0}]", string.Join(", ", numtostr));
        }
    }
}
